unit UnParser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, IdBaseComponent, IdComponent,
  IdTCPServer, IdCustomHTTPServer, IdHTTPServer, IdHTTP, IdTCPConnection,
  IdTCPClient, superobject, StdCtrls, uLkJSON;

type
  TForm1 = class(TForm)
    IdHTTPServer1: TIdHTTPServer;
    IdHTTP1: TIdHTTP;
    Label1: TLabel;
    procedure IdHTTPServer1CommandGet(AThread: TIdPeerThread;
      ARequestInfo: TIdHTTPRequestInfo;
      AResponseInfo: TIdHTTPResponseInfo);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure ReadJSON();
begin
end;

procedure TForm1.IdHTTPServer1CommandGet(AThread: TIdPeerThread;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
var
  unparsed_parameters: String;
  updated_string: UTF8String;
  ansi_string: String;

  json_data, item: ISuperObject;
  json_object: TlkJSONobject;
  json_students: TlkJSONlist;

  i: integer;
begin
  unparsed_parameters := ARequestInfo.UnparsedParams;

  //-- Super Object --//
  json_data := SO(unparsed_parameters);
  for i := 0 to json_data.A['students'].Length-1 do
  begin
    item := json_data.A['students'].O[i];
    OutputDebugString(PAnsiChar('Student name: ' + item.S['fullname']));
  end;
  
  //-- lkJSON --//
  //json_object := TlkJSON.ParseText(unparsed_parameters) as TlkJSONobject;
  //ShowMessage(json_object.Field['students']);
  //json_students := json_object.Field['students'].Value;
  //ShowMessage(json_students.Child[0].Field['fullname'].Value);
end;

end.
